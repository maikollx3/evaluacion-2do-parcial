package facci.pm.LucasHolguinMichael;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Base_Ruta extends SQLiteOpenHelper {

    String tabla="CREATE TABLE RUTAS(ID INTEGER PRIMARY KEY, RUTA TEXT, ORIGEN TEXT, DESTINO TEXT, COMPANIA TEXT, TIEMPO TEXT)";
    public Base_Ruta(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tabla);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table rutas");
        db.execSQL(tabla);

    }
}
