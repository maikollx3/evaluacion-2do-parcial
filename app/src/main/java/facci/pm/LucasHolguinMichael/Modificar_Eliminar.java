package facci.pm.LucasHolguinMichael;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Modificar_Eliminar extends AppCompatActivity {

    EditText txtruta,txtorigen,txtdestino,txtcompaniea,txttiempo;
    Button btnmodificar,btneliminar;

    int id;
    String ruta;
    String origen;
    String destino;
    String compania;
    String tiempo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar__eliminar);
        Bundle b = getIntent().getExtras();
        if (b!=null){
            id =b.getInt("Id");
            ruta=b.getString("Ruta");
            origen=b.getString("Origen");
            destino=b.getString("Destino");
            compania=b.getString("Compania");
            tiempo=b.getString("Tiempo");
        }

        txtruta=findViewById(R.id.rutas);
        txtorigen=findViewById(R.id.origen);
        txtdestino=findViewById(R.id.destino);
        txtcompaniea=findViewById(R.id.compa);
        txttiempo=findViewById(R.id.tiempo);
        btnmodificar=findViewById(R.id.btnmodificar);
        btneliminar=findViewById(R.id.btneliminar);

        txtruta.setText(ruta);
        txtorigen.setText(origen);
        txtdestino.setText(destino);
        txtcompaniea.setText(compania);
        txttiempo.setText(tiempo);

        btneliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Eliminar(id);
                onBackPressed();

            }

        });

        btnmodificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Modificar(id,txtruta.getText().toString(),txtorigen.getText().toString(),txtdestino.getText().toString(),txtcompaniea.getText().toString(),txttiempo.getText().toString());
                onBackPressed();

            }
        });
    }

    private void Modificar(int Id,String Ruta, String Origen, String Destino, String Compania, String Tiempo){
        Base_Ruta rutas = new Base_Ruta(this, "DbRutas",null,1);
        SQLiteDatabase db = rutas.getWritableDatabase();

        String sql ="update Rutas set Ruta='"+ Ruta +"',Origen='"+ Origen + "',Destino='"+ Destino + "',Compania='"+ Compania + "',Tiempo='"+ Tiempo +"' where Id="+Id;
        db.execSQL(sql);
        db.close();
    }

    private void Eliminar(int Id){
        Base_Ruta rutas = new Base_Ruta(this, "DbRutas",null,1);
        SQLiteDatabase db = rutas.getWritableDatabase();

        String sql = "delete from Rutas where Id="+Id;
        db.execSQL(sql);
        db.close();
    }
}
