package facci.pm.LucasHolguinMichael;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText ruta, origen, destino, companiea, tiempo;
    Button btnguardar, btnbuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ruta=findViewById(R.id.rutas);
        origen=findViewById(R.id.origen);
        destino=findViewById(R.id.destino);
        companiea=findViewById(R.id.compa);
        tiempo=findViewById(R.id.tiempo);
        btnguardar=findViewById(R.id.btnnsave);
        btnbuscar=findViewById(R.id.btnbuscar);

        btnguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardar(ruta.getText().toString(),origen.getText().toString(),destino.getText().toString(),companiea.getText().toString(),tiempo.getText().toString());
            }
        });
        btnbuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,Lista.class));
            }
        });
    }
    private void guardar(String Ruta, String Origen, String Destino, String Compania, String Tiempo){
        Base_Ruta rutas = new Base_Ruta(this, "DbRutas",null,1);
        SQLiteDatabase db = rutas.getWritableDatabase();
        try {
            ContentValues c = new ContentValues();
            c.put("Ruta",Ruta);
            c.put("Origen",Origen);
            c.put("Destino",Destino);
            c.put("Compania",Compania);
            c.put("Tiempo",Tiempo);
            db.insert("RUTAS","",c);
            db.close();
            Toast.makeText(this,"Registro insertado",Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            Toast.makeText(this,"Error:" + e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }


}
